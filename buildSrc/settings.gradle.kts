enableFeaturePreview("VERSION_CATALOGS")

rootProject.name = "buildSrc"

@Suppress("UnstableApiUsage")
dependencyResolutionManagement {

    versionCatalogs {
        create("libs") {
            from(files("../gradle/libs.versions.toml"))
        }
    }

    repositories {
        google()
        mavenCentral()
    }
}