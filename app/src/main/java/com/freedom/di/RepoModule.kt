package com.freedom.di

import android.util.Log
import com.freedom.data.StocksRepo
import com.freedom.data.StocksRepoImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.kotlinx.serializer.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.plugins.observer.*
import io.ktor.client.plugins.websocket.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepoModule {

    companion object {
        @Provides
        @Singleton
        fun ktorHttpClient(): HttpClient = HttpClient(OkHttp) {

            System.setProperty("kotlinx.coroutines.debug", "on")

            expectSuccess = true

            install(ContentNegotiation) {
                json()

                engine {
                    config {
                        connectTimeout(15, TimeUnit.SECONDS)
                    }
                }
            }

            install(Logging) {
                logger = object : Logger {
                    override fun log(message: String) {
                        Timber.v(Thread.currentThread().name + " " + message)
                    }
                }
                level = LogLevel.ALL
            }

            install(WebSockets)

            install(DefaultRequest) {
                header(HttpHeaders.ContentType, ContentType.Application.Json)
            }
        }
    }

    @Binds
    abstract fun bindRepo(impl: StocksRepoImpl) : StocksRepo
}