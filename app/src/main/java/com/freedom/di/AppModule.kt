package com.freedom.di

import com.freedom.AppConfig
import com.freedom.BuildConfig
import com.freedom.ui.theme.Formatter
import com.freedom.ui.theme.FormatterImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    companion object {
        @Provides
        @Singleton
        fun appConfig() = AppConfig(BuildConfig.baseUrl)
    }

    @Binds
    abstract fun bind(impl: FormatterImpl): Formatter
}