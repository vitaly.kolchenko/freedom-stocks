package com.freedom.ui.theme

import androidx.compose.material3.ColorScheme
import androidx.compose.runtime.Immutable
import androidx.compose.ui.graphics.Color

@Immutable
data class FreedomColorScheme(
    val material: ColorScheme,

    val secondaryOnBackground: Color,
    val divider: Color,
    val stocksUp: Color,
    val stocksDown: Color
) {
    val primaryText: Color get() = material.onBackground
    val primaryTextInverse: Color get() = material.background
}