package com.freedom.ui

import androidx.compose.animation.Animatable
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector4D
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import coil.compose.AsyncImage
import coil.compose.AsyncImagePainter
import com.freedom.ui.theme.freedomColorScheme
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

@Composable
fun StockCard(
    stockItem: StockItemUi,
    isTickerBlinked: Boolean,
    setTickerBlinked: (String, Double) -> Unit
) {
    ConstraintLayout(modifier = Modifier
        .fillMaxWidth()
        .clickable { }
        .padding(8.dp)) {

        val (title1, image, title2, subtitle1, subtitle2, arrow) = createRefs()

        var invalidImage by remember { mutableStateOf(false) }
        AsyncImage(model = stockItem.iconUrl, contentDescription = null, contentScale = ContentScale.Inside, onState = {
            if (isImageInvalid(it)) {
                invalidImage = true
            }
        }, modifier = Modifier
            .constrainAs(image) {
                top.linkTo(title1.top)
                bottom.linkTo(title1.bottom)
                start.linkTo(parent.start)
                width = if (!invalidImage) {
                    Dimension.ratio("1:1")
                } else {
                    Dimension.value(0.dp)
                }
                height = Dimension.fillToConstraints
            }
            .padding(end = if (!invalidImage) 8.dp else 0.dp))

        Text(text = stockItem.c,
            color = MaterialTheme.freedomColorScheme.primaryText,
            style = MaterialTheme.typography.titleLarge,
            modifier = Modifier.constrainAs(title1) {
                start.linkTo(image.end)
            })

        Text(text = "${stockItem.ltr} | ${stockItem.name}",
            color = MaterialTheme.freedomColorScheme.secondaryOnBackground,
            style = MaterialTheme.typography.labelMedium,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.constrainAs(subtitle1) {

                linkTo(
                    start = parent.start,
                    end = subtitle2.start,
                    top = title1.bottom,
                    bottom = parent.bottom,
                    topMargin = 4.dp,
                    endMargin = 4.dp,
                    horizontalBias = 0f
                )

                width = Dimension.fillToConstraints
            })

        PcpField(
            ticker = stockItem.c,
            pcp = stockItem.pcp,
            pcpDiff = stockItem.pcpDiff,
            stockItem.pcpFmt,
            modifier = Modifier.constrainAs(title2) {
                top.linkTo(parent.top)
                end.linkTo(arrow.start)
            }, isTickerBlinked, setTickerBlinked
        )

        Text(text = "${stockItem.ltpFmt} ( ${stockItem.chgFmt} )",
            color = MaterialTheme.freedomColorScheme.primaryText,
            style = MaterialTheme.typography.labelMedium,
            modifier = Modifier.constrainAs(subtitle2) {
                end.linkTo(arrow.start)

                bottom.linkTo(parent.bottom)
            })

        Image(
            imageVector = Icons.Default.KeyboardArrowRight,
            contentDescription = null,
            modifier = Modifier.constrainAs(arrow) {
                end.linkTo(parent.end)
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
            },
            colorFilter = ColorFilter.tint(MaterialTheme.freedomColorScheme.secondaryOnBackground)
        )
    }
}

private const val blinkDuration = 200

@Composable
fun PcpField(
    ticker: String,
    pcp: Double,
    pcpDiff: Double,
    formattedPcp: String,
    modifier: Modifier,
    isTickerBlinked: Boolean,
    setTickerBlinked: (String, Double) -> Unit
) {

    val currentSetTickerBlinked by rememberUpdatedState(setTickerBlinked)

    val pcpBgBlinkColor =
        when {
            pcpDiff < 0 -> MaterialTheme.freedomColorScheme.stocksDown
            else -> MaterialTheme.freedomColorScheme.stocksUp
        }
    val pcpBgColor = pcpBgBlinkColor.copy(alpha = 0f)
    val pcpBgColorAnimated = remember { Animatable(pcpBgColor) }
    val pcpTextColor = when {
        pcp == 0.0 -> MaterialTheme.freedomColorScheme.primaryText
        pcp > 0 -> MaterialTheme.freedomColorScheme.stocksUp
        pcp < 0 -> MaterialTheme.freedomColorScheme.stocksDown
        else -> MaterialTheme.freedomColorScheme.stocksUp
    }
    val pcpTextColorAnimated = remember { Animatable(pcpTextColor) }
    val blinkPcpTextColor = MaterialTheme.freedomColorScheme.primaryTextInverse

    if (!isTickerBlinked) {
        LaunchedEffect(pcpDiff) {
            coroutineScope {
                if (pcpDiff != 0.0) {
                    launch {
                        pcpBgColorAnimated.animateToWithCleanup(pcpBgBlinkColor, pcpBgColor)
                    }
                    launch {
                        pcpTextColorAnimated.animateToWithCleanup(blinkPcpTextColor, pcpTextColor)
                    }
                } else {
                    launch {
                        pcpBgColorAnimated.animateToWithCleanup(pcpBgColor)
                    }
                    launch {
                        pcpTextColorAnimated.animateToWithCleanup(pcpTextColor)
                    }
                }
            }
            currentSetTickerBlinked(ticker, pcpDiff)
        }
    }

    Surface(shape = RoundedCornerShape(8.dp), color = pcpBgColorAnimated.value, modifier = modifier) {
        Text(
            text = "${formattedPcp}%",
            style = MaterialTheme.typography.titleLarge,
            color = pcpTextColorAnimated.value,
            modifier = Modifier.padding(horizontal = 4.dp),
        )
    }
}

private suspend fun Animatable<Color, AnimationVector4D>.animateToWithCleanup(
    color1: Color,
    color2: Color? = null
) {
    try {
        animateTo(color1, animationSpec = tween(blinkDuration))
        if (color2 != null) animateTo(color2, animationSpec = tween(blinkDuration))
    } finally {
        snapTo(color2 ?: color1)
    }
}

private fun isImageInvalid(it: AsyncImagePainter.State) =
    it is AsyncImagePainter.State.Success && it.result.drawable.intrinsicHeight <= 2