package com.freedom.ui

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.repeatOnLifecycle
import com.freedom.R
import com.freedom.ui.theme.freedomColorScheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@OptIn(ExperimentalLifecycleComposeApi::class)
@Composable
fun StocksScreen(viewModel: StocksViewModel) {
    val state by viewModel.state.collectAsStateWithLifecycle()

    val lifecycleOwner = LocalLifecycleOwner.current
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(key1 = Unit) {
        viewModel.getStocksOnStart(lifecycleOwner)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.freedomColorScheme.material.background)
    ) {

        state.onSuccess {
            StocksList(stocks = it)
        }.onFailure {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show()
            ErrorScreen(viewModel = viewModel, throwable = it, lifecycleOwner, coroutineScope)
        }
    }
}

@Composable
fun ErrorScreen(
    viewModel: StocksViewModel,
    throwable: Throwable,
    lifecycleOwner: LifecycleOwner,
    coroutineScope: CoroutineScope
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Button(onClick = {
            coroutineScope.launch { viewModel.getStocksOnStart(lifecycleOwner) }
        }) {
            Image(
                imageVector = Icons.Default.Refresh,
                contentDescription = "Retry icon",
                colorFilter = ColorFilter.tint(MaterialTheme.freedomColorScheme.material.onPrimary)
            )
            Spacer(modifier = Modifier.size(12.dp))
            Text(stringResource(R.string.retry))
        }
    }
}

private suspend fun StocksViewModel.getStocksOnStart(lifecycleOwner: LifecycleOwner) {
    lifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
        getStocks(symbols)
    }
}

private val symbols = listOf(
    "RSTI",
    "GAZP",
    "MRKZ",
    "RUAL",
    "HYDR",
    "MRKS",
    "SBER",
    "FEES",
    "TGKA",
    "VTBR",
    "ANH.US",
    "VICL.US",
    "BURG.US",
    "NBL.US",
    "YETI.US",
    "WSFS.US",
    "NIO.US",
    "DXC.US",
    "MIC.US",
    "HSBC.US",
    "EXPN.EU",
    "GSK.EU",
    "SHP.EU",
    "MAN.EU",
    "DB1.EU",
    "MUV2.EU",
    "TATE.EU",
    "KGF.EU",
    "MGGT.EU",
    "SGGD.EU"
)
