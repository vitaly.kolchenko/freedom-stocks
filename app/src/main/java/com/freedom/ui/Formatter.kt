package com.freedom.ui.theme

import android.content.Context
import androidx.core.os.ConfigurationCompat
import dagger.hilt.android.qualifiers.ApplicationContext
import java.text.DecimalFormat
import java.text.FieldPosition
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

interface Formatter {
    fun formatPcp(value: Double): String
    fun formatLtp(value: Double): String
    fun formatChg(value: Double): String
}

class FormatterImpl @Inject constructor(@ApplicationContext context: Context) : Formatter {

    private val locale =
        ConfigurationCompat.getLocales(context.resources.configuration).get(0) ?: Locale.getDefault();

    private val pcpFormat: DecimalFormat = getFormat("0.00")
    private val ltpFormat: DecimalFormat = getFormat("0.00#")
    private val cngFormat: DecimalFormat = getFormat("0.00#")

    private fun getFormat(pattern: String) = (NumberFormat.getNumberInstance(locale) as DecimalFormat).apply {
        applyPattern(pattern)
    }

    init {

        val maxFraction = 50

        pcpFormat.negativePrefix = ""

        ltpFormat.maximumFractionDigits = maxFraction

        cngFormat.maximumFractionDigits = maxFraction
        cngFormat.negativePrefix = ""
    }

    override fun formatPcp(value: Double): String {
        return pcpFormat.format(value, StringBuffer(sign(value)), FieldPosition(DecimalFormat.INTEGER_FIELD)).toString()
    }

    private fun sign(value: Double) = if (value > 0) "+" else if (value < 0) "-" else ""

    override fun formatLtp(value: Double): String {
        return ltpFormat.format(value)
    }

    override fun formatChg(value: Double): String {
        return cngFormat.format(value, StringBuffer(sign(value)), FieldPosition(DecimalFormat.INTEGER_FIELD)).toString()
    }
}