package com.freedom.ui

data class StockItemUi(
    val c: String = "",
    val pcp: Double = 0.0,
    val pcpDiff: Double = 0.0,
    val pcpFmt: String = "",
    val ltr: String = "",
    val name: String = "",
    val ltpFmt: String = "",
    val chgFmt: String = "",
    val iconUrl: String = ""
)
