package com.freedom.ui

import androidx.lifecycle.ViewModel
import com.freedom.AppConfig
import com.freedom.data.StockItem
import com.freedom.data.StocksRepo
import com.freedom.ui.theme.Formatter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class StocksViewModel @Inject constructor(
    private val stocksRepo: StocksRepo,
    private val formatter: Formatter,
    private val appConfig: AppConfig
) : ViewModel() {

    private val _state: MutableStateFlow<Result<List<StockItemUi>>> = MutableStateFlow(Result.success(emptyList()))
    val state: StateFlow<Result<List<StockItemUi>>> = _state

    suspend fun getStocks(symbols: List<String>) {
            try {
                stocksRepo.getStocks(symbols).map {
                    it.map(::processItem)
                }.collect { ll -> _state.value = Result.success(ll) }
            } catch (e: IOException) {
                Timber.e(e)
                _state.value = Result.failure(e)
            }
    }

    private fun processItem(it: StockItem) = StockItemUi(
        it.c,
        it.pcp ?: 0.0,
        it.pcpDiff ?: 0.0,
        formatter.formatPcp(it.pcp ?: 0.0),
        it.ltr ?: "",
        it.name ?: "",
        formatter.formatLtp(it.ltp ?: 0.0),
        formatter.formatChg(it.chg ?: 0.0),
        "https://${appConfig.baseUrl}/logos/get-logo-by-ticker?ticker=${it.c.lowercase()}"
    )
}