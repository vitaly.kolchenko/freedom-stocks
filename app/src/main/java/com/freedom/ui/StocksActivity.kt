package com.freedom.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.core.view.WindowCompat
import com.freedom.ui.theme.StocksTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StocksActivity : ComponentActivity() {
    private val vm by viewModels<StocksViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            StocksTheme {
                StocksScreen(vm)
            }
        }
    }
}