package com.freedom.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.asPaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBars
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.freedom.ui.theme.freedomColorScheme

@Composable
fun StocksList(stocks: List<StockItemUi>) {

    val diffBlinkState = remember { mutableStateMapOf<String, Double>() }

    LazyColumn(
        contentPadding = WindowInsets.systemBars.asPaddingValues()
    ) {
        itemsIndexed(stocks) { i, item ->
            StockCard(
                item,
                diffBlinkState[item.c] == item.pcpDiff
            ) { ticker, pcpDiff -> diffBlinkState[ticker] = pcpDiff }
            if (i != stocks.lastIndex) {
                Divider(
                    thickness = 0.5.dp,
                    modifier = Modifier
                        .padding(start = 12.dp)
                        .background(MaterialTheme.freedomColorScheme.divider)
                )
            }
        }
    }
}