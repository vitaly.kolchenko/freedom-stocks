package com.freedom.data

import kotlinx.coroutines.flow.Flow

interface StocksRepo {
    suspend fun getStocks(tickers: List<String>): Flow<List<StockItem>>
}


