package com.freedom.data

@kotlinx.serialization.Serializable
data class StockItem(
    /**
     * тикер
     */
    val c: String = "",
    /**
     * Изменение в процентах относительно цены закрытия предыдущей торговой сессии
     */
    val pcp: Double? = null,
    /**
     * Биржа последней сделки
     */
    val ltr: String? = null,

    val name: String? = null,

    /**
     * Цена последней сделки
     */
    val ltp: Double? = null,

    /**
     * Изменение цены последней сделки в пунктах относительно цены закрытия предыдущей торговой сессии
     */
    val chg: Double? = null,

    val pcpDiff: Double? = null,

    val min_step: Double? = null
)