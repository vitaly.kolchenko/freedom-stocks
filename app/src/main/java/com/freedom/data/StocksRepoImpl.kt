package com.freedom.data

import com.freedom.AppConfig
import io.ktor.client.*
import io.ktor.client.plugins.websocket.*
import io.ktor.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val RETRIES = 3L

class StocksRepoImpl @Inject constructor(
    private val httpClient: HttpClient,
    private val jsonParser: JsonParser,
    private val appConfig: AppConfig
) : StocksRepo {

    override suspend fun getStocks(tickers: List<String>): Flow<List<StockItem>> = withContext(Dispatchers.IO) {
        return@withContext flow {
            val stocksByTicker: LinkedHashMap<String, StockItem> = linkedMapOf()

            val session = httpClient.webSocketSession("wss://wss.${appConfig.baseUrl}")

            val request = """["quotes", [${tickers.joinToString(",", transform = { "\"$it\"" })}]]"""
            session.send(request)
            while (true) {
                session.incoming.receive().let { jsonParser.parseFrame(it, stocksByTicker) }
                    ?.let {
                        stocksByTicker[it.c] = it;
                        stocksByTicker.values.toList()
                    }?.let {
                        //Collect all tickers before first emit
                        if (stocksByTicker.size == tickers.size)
                            emit(it)
                    }
            }
        }.retry(RETRIES)
    }

}


