package com.freedom.data

import io.ktor.websocket.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonPrimitive
import timber.log.Timber
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.roundToInt

@Singleton
class JsonParser @Inject constructor() {
    @OptIn(ExperimentalSerializationApi::class)
    private val json = Json {
        ignoreUnknownKeys = true
        explicitNulls = false
    }

    fun parseFrame(frame: Frame, state: MutableMap<String, StockItem>): StockItem? {
        val jsonArray = json.decodeFromString<JsonArray>(frame.readBytes().decodeToString())
        val type = jsonArray.getOrNull(0)?.jsonPrimitive?.content

        if (type != "q") return null

        val payload = jsonArray.getOrNull(1)?.let { json.decodeFromJsonElement<StockItem>(it) } ?: return null

        Timber.d(payload.toString())

        val ticker = payload.c
        val currentStock = state.getOrPut(ticker) { payload }
        val updated = payload.run {
            currentStock.copy(
                c = currentStock.c,
                pcp = pcp ?: currentStock.pcp,
                ltr = ltr ?: currentStock.ltr,
                name = name ?: currentStock.name,
                ltp = ltp ?: currentStock.ltp,
                chg = chg ?: currentStock.chg,
                pcpDiff = computePcpDiff(currentStock.pcp, pcp, min_step ?: currentStock.min_step)
            )
        }

        return updated
    }

    private fun computePcpDiff(lastValue: Double?, newValue: Double?, minStep: Double?): Double {
        if (lastValue != null && newValue != null) {
            val diff = newValue - lastValue
            if (minStep != null && minStep != 0.0) {
                return (diff / minStep).roundToInt() * minStep
            }
            return diff
        } else return 0.0
    }
}