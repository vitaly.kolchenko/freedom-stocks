plugins {
    id("android-application-convention")
    id("kotlin-kapt-convention")
    kotlin("plugin.serialization")
}

android {
    defaultConfig {
        applicationId = "com.freedom"
        namespace = "com.freedom"
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }

    signingConfigs {
        create("release") {
            storeFile = file("../debug_keystore")
            storePassword = "123456"
            keyAlias = "key0"
            keyPassword ="123456"
        }

        getByName("debug") {
            storeFile = file("../debug_keystore")
            storePassword = "123456"
            keyAlias = "key0"
            keyPassword ="123456"
        }
    }

    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
        }
    }

    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.composeCompiler.get()
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation(libs.bundles.compose)
    implementation(libs.bundles.ktor)
    implementation(libs.bundles.androidx)
    implementation(libs.timber)
    implementation(libs.mviorbit.viewmodel)

    testImplementation(libs.test.junit)
    androidTestImplementation(libs.androidx.test.junit.ext)
    androidTestImplementation(libs.androidx.test.espresso)

    androidTestImplementation(libs.compose.junit)
    debugImplementation(libs.compose.tooling)
    debugImplementation(libs.compose.test.manifest)
}
