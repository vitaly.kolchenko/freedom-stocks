enableFeaturePreview("VERSION_CATALOGS")
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

rootProject.name = "Stocks"

include(":app")

pluginManagement {

    repositories {
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")//for kapt plugin 1.5.30
    }
}

dependencyResolutionManagement {

    repositories {
        google()
        mavenCentral()
    }
}


